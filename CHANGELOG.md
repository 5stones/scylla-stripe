<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/5stones/scylla-stripe/compare/v1.0.0...v1.1.0) (2019-01-18)


### Features

* **StripeTask:** Link all the subrecords back to the parent ([a15a468](https://gitlab.com/5stones/scylla-stripe/commit/a15a468))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/5stones/scylla-stripe/compare/ad7997b...v1.0.0) (2019-01-17)


### Features

* ***/*:** Add basic functionality to download newly created payout records from Stripe ([ad7997b](https://gitlab.com/5stones/scylla-stripe/commit/ad7997b))



