from scylla import tasks, orientdb
from scylla import ErrorLogging
from .records import StripeRecord
from datetime import datetime
from pprint import pprint
import stripe


class StripeTask(tasks.Task):
    """Downloads records from Stripe.
    """

    def __init__(self, client, record_type, record_prefix='Stripe', page_size=30):
        self.record_type = record_type
        self.client = client
        self.classname = '{0}{1}'.format(record_prefix, self.record_type)
        self.page_size = page_size

        task_id = '{0}-download'.format(self.classname)
        super(StripeTask, self).__init__(task_id)

    def _step(self, after, options):
        created = None
        if after:
            # convert after to epoch
            after_epoch = datetime.strptime(after, '%Y-%m-%dT%H:%M:%SZ').strftime('%s')
            created = { 'gt': after_epoch }

        print("\nGetting {0}(s) updated after {1} :".format(self.record_type, after))
        objs = getattr(self.client, self.record_type).list(limit=self.page_size, created=created)

        # iterate/paginate over stripe objects
        for obj in objs.auto_paging_iter():
            self._process_response_record(obj)

    def process_one(self, rec_id):
        response = getattr(self.client, self.record_type).retrieve(rec_id)
        self._process_response_record(response)

    def _process_response_record(self, obj):
        rec = StripeRecord.factory(self.client, self.record_type, obj)
        with ErrorLogging(self.task_id, rec):
            rec.throw_at_orient()
            self._link_children(rec)
            print("Saved {} {} as {} {}".format(
                    self.record_type,
                    obj.get(rec.key_field),
                    rec.classname,
                    rec.rid
                )
            )

    def _link_children(self, rec):
        """Once the record and subrecords are in orientdb, add _parent to children
            and children of children recursively, depth first.
        """
        # pull rids of children
        rids = []
        for subtype in rec._sub_records:
            value = rec.get(subtype)
            if isinstance(value, StripeRecord):
                # single record
                rids.append(value.rid)
                # recursively link children of child
                self._link_children(value)
            elif value:
                # list of records
                for subrecord in value:
                    rids.append(subrecord.rid)
                    # recursively link children of child
                    self._link_children(subrecord)

        # run the update in orientdb
        q = "UPDATE [{}] SET _parent = {}".format(','.join(rids), rec.rid)
        orientdb.execute(q)
