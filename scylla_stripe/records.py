from datetime import datetime
from scylla import records
import stripe

# recursion depth when initializing records
_process_depth = 0

# cache of records cleared after processing a response
_record_cache = {}


class StripeRecord(records.ParsedRecord):
    key_field = 'id'

    factory_base_classname = 'Stripe'
    classname = 'StripeRecord'

    # stripe id's are strings, not ints
    _int_fields = []
    _sub_records = {}
    _linked_records = {}
    _datetime_fields = ['created']

    @classmethod
    def factory(cls, client, classname, obj, readonly_link=False):
        """Extends default factory behavior to iterate over stripe ListObjects
        """
        if obj is None:
            return None

        # search for a record type from a module's base record type
        base_cls = cls._record_base_cls or cls
        subcls = base_cls.get_subclass(classname)

        # subcls.factory may be called, so make it search from the same base
        subcls._record_base_cls = base_cls

        # iterate through stripe list objects
        if isinstance(obj, stripe.ListObject):
            return [subcls.process_obj(client, classname, d, readonly_link) for d in obj.auto_paging_iter()]

        return super(StripeRecord, cls).factory(client, classname, obj, readonly_link=readonly_link)

    def _process_fields(self):

        # pre-process datetimes
        for field in self._datetime_fields:
            if field in self.data:
                self.data[field] = self._convert_date_format(self.data[field])

        super(StripeRecord, self)._process_fields()

    def _convert_date_format(self, date):

        if date is None or date == '':
            return date

        # convert epoch into timestamp
        d = datetime.fromtimestamp(date)
        return '{}Z'.format(d.isoformat())


class StripeAccount(StripeRecord):
    pass


class StripeTransaction(StripeRecord):
    pass


class StripePayout(StripeRecord):
    _datetime_fields =  StripeRecord._datetime_fields + ['arrival_date']

    _sub_records = {
        'account': 'Account',
        'bank_account': 'Account',
        'transactions': 'Transaction',
    }
