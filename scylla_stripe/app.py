from scylla import app
from scylla import configuration
import stripe
from .tasks import StripeTask

configuration.setDefaults('Stripe', {
    #'api_key': None,
    'debug': False,
})


class App(app.App):

    task_params = [
        ('Payout',),
    ]

    on_demand_task_params = [
    ]

    def _prepare(self):
        config = configuration.getSection('Stripe')
        self.client = stripe
        # force set a name on the client as areas of the app require it
        # it's a bit of a hack
        self.client.name = config.get('name')
        self.client.api_key = config.get('api_key')

        for task_params in self.task_params:
            task = StripeTask(self.client, *task_params)
            self.tasks[task_params[0]] = task

        for task_params in self.on_demand_task_params:
            task = StripeTask(self.client, *task_params)
            self.on_demand_tasks[task_params[0]] = task
